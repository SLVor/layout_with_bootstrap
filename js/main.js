

$('.carousel').carousel({
	cycle:true
});

$(window).bind("load", function() {   
    var footerHeight = 0,
        footerTop = 0,
        $footer = $("footer");
    positionFooter();
    function positionFooter() {
        footerHeight = $footer.height();
        footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
        
        if ( ($(document.body).height()+(footerHeight * 0.6)) < $(window).height()) {
            $footer.css({
                position: "fixed",
                bottom: 0,
                right: 0,
                left: 0
            });
        } else {
            $footer.css({
                position: "static"
            });
        }
    }
    $(window).scroll(positionFooter).resize(positionFooter);
});
